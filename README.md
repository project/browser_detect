# Browser Detect

This project allows you to identify the browser and the device used. A class
will be added to the HTML tag.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/browser_detect).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/browser_detect).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. See
[Installing Drupal Modules](https://drupal.org/documentation/install/modules-themes/modules-8) for further information.


## Configuration

- There is no configuration.


## Maintainers

- [Rafael Nica (rafael.nica)](https://www.drupal.org/user/1067692)
- [Renato Gonçalves H (renatog)](https://www.drupal.org/u/renatog)
- [Murilo (murilohp)](https://www.drupal.org/u/murilohp)
