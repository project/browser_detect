(function (Drupal, $) {
  'use strict';

  Drupal.behaviors.browser_detect = {
    browserVersion: function () {
      const userBrowser = (navigator.userAgent).toLowerCase();
      const maxTouchPoints = navigator.maxTouchPoints;
      let $html = document.getElementsByTagName('html')[0],
        browsers = [],
        systemOp = [],
        matches;

      // Browser
      browsers.safari = userBrowser.indexOf('safari');
      browsers.chrome = (userBrowser.indexOf('chrome') > -1 || userBrowser.indexOf('crios') > -1 ) ? 1 : -1;
      browsers.firefox = userBrowser.indexOf('firefox');
      browsers.ie = userBrowser.indexOf('msie');
      browsers.ie11 = userBrowser.indexOf('rv:11');
      browsers.msedge = userBrowser.indexOf('edg');
      browsers.samsung = userBrowser.indexOf('samsung');
      browsers.ucbrowser = userBrowser.indexOf('ucbrowser');
      browsers.facebook = (userBrowser.indexOf('fbdv') > -1 || userBrowser.indexOf('fb_iab') > -1 ) ? 1 : -1;
      browsers.instagram = userBrowser.indexOf('instagram');

      //SO
      systemOp.linux = userBrowser.indexOf('linux');
      systemOp.macintosh = (userBrowser.indexOf('macintosh') > -1 && maxTouchPoints === 0) ? 1 : -1;
      systemOp.windows = userBrowser.indexOf('windows');
      systemOp.ipad = (userBrowser.indexOf('ipad') > -1) || (userBrowser.indexOf('macintosh') && maxTouchPoints > 0) ? 1 : -1;
      systemOp.android = userBrowser.indexOf('android');
      systemOp.iphone = userBrowser.indexOf('iphone');

      //Chrome, Safari, Edge, Samsung, UC, Facebook & Instagram className
      if(browsers.samsung > -1) {
        $html.classList.add('samsungbrowser');
      } else if(browsers.ucbrowser > -1) {
        $html.classList.add('ucbrowser');
      } else if(browsers.facebook > -1) {
        $html.classList.add('facebook');
      } else if(browsers.instagram > -1) {
        $html.classList.add('instagram');
      } else if ((browsers.safari > -1) && (browsers.chrome <= -1) && (browsers.msedge <= -1)) {
        $html.classList.add('safari');
        matches = userBrowser.match(/version\/([0-9]*)/);
        $html.classList.add(`safari${matches[1]}`);
      } else if ((browsers.chrome > -1) && (browsers.msedge <= -1)) {
        $html.classList.add('chrome');
        matches = userBrowser.match(/chrome\/([0-9]*)|crios\/([0-9]*)/);
        $html.classList.add(`chrome${matches[1]}`);
      } else if (browsers.msedge > -1) {
        $html.classList.add('edge');
        matches = userBrowser.match(/edge\/([0-9]*)|edg\/([0-9]*)/);
        $html.classList.add(`edge${matches[1]}`);
      }

      //firefox
      if (browsers.firefox > -1) {
        $html.classList.add('firefox');
        matches = userBrowser.match(/firefox\/([0-9]*)/);
        $html.classList.add(`firefox${matches[1]}`);
      }

      //IE
      if (browsers.ie > -1 || browsers.ie11 > -1) {
        $html.classList.add('ie');
        if (browsers.ie > -1) {
          matches = userBrowser.match(/msie\ ([0-9]*)/);
          $html.classList.add(`ie${matches[1]}`);
        } else {
          //IE11
          $html.classList.add('ie11');
        }
      }

      //Windows
      if (systemOp.windows > -1) {
        $html.classList.add('windows');
        $html.classList.add('desktop');
      }
      //macintosh
      if (systemOp.macintosh > -1) {
        $html.classList.add('macintosh');
        $html.classList.add('desktop');
      }
      //linux
      if (systemOp.linux > -1 && systemOp.android <= -1) {
        $html.classList.add('linux');
        $html.classList.add('desktop');
      }
      //android
      if (systemOp.android > -1) {
        $html.classList.add('android');
        $html.classList.add('mobile');
      }
      //ipad
      if (systemOp.ipad > -1) {
        $html.classList.add('ipad');
        $html.classList.add('mobile');
        $html.classList.add('ios');
      }
      //iphone
      if (systemOp.iphone > -1) {
        $html.classList.add('iphone');
        $html.classList.add('mobile');
        $html.classList.add('ios');
      }
    },

    attach: function (context) {
      const hasHtmlTag = $('html', context).length;
      if (hasHtmlTag) {
        Drupal.behaviors.browser_detect.browserVersion();
      }
    }
  };

})(Drupal, jQuery);
